# On Cloud 10

A cloud-based Phase 10 scoring web application designed to ease the task of keeping score on paper.

## Tools Used
This project uses Bootstrap 5 as its front-end framework and Django as its back-end framework. User data is hosted in a Postgres Docker container. The web-app itself is hosted in a Digital Ocean Kubernetes cluster, where a Nginx load balancer's external IP-address is linked to <https://oncloud10.xyz>.

## Environment
Make a `.env` file in the root directory and set the following variables:

- POSTGRES_DB
- POSTGRES_ADMIN
- POSTGRES_PASSWORD
- PGDATA='/data/phase10'

***In `settings.py`, don't forget to switch debug mode to `True`, comment out the allowed hosts and csrf trusted origins, and to use localhost for Postgres*

## Local Installation
Set up a virtual environment and install the requirements by running `pip install -r requirements.txt`. Make sure you have Docker and Docker compose installed on your machine. Create a Docker volume by running `docker volume create --name=phase10`. Run `docker compose up` to install a Postgres docker container locally.

Once your Postgres container is running on port 5432, run the following commands:

- `python manage.py makemigrations`
- `python manage.py migrate`
- `python manage.py runserver`

## License
The favicon is marked with CC0 1.0 Universal.
