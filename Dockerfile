FROM python:3.8

WORKDIR /app

ENV PYTHONUNBUFFERED = 1

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY /on_cloud_10 .

EXPOSE 8000

RUN python manage.py collectstatic --no-input

CMD ["gunicorn", "on_cloud_10.wsgi:application", "--bind", "0.0.0.0:8000", "--workers", "5"]