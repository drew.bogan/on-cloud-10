"""on_cloud_10 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from postgres import views

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('', views.home_page, name='home_page'),
    path('game/', views.new_game, name='new_game'),
    path('game/<int:game_id>/', views.load_game, name='load_game'),
    path('validate_game/<int:game_id>/', views.validate_game_code, name='validate_game'),
    path('save_game/', views.save_players_progress, name="save_players_progress")
]
