from django import forms
from crispy_bootstrap5.bootstrap5 import FloatingField
from crispy_forms.layout import Layout, Div
from crispy_forms.helper import FormHelper
from .models import Player

# class PlayerForm(forms.Form):
#     name = forms.CharField(max_length=250)

#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.helper = FormHelper(self)
#         self.helper.form_class = 'form-floating mb-3'