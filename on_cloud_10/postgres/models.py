from django.db import models

# On Cloud 10 models (tables)

class Game(models.Model):
    date_of_game = models.DateTimeField()

class Player(models.Model):
    name = models.CharField(max_length=50)
    score = models.IntegerField()
    phase = models.IntegerField()
    game = models.ForeignKey("Game", on_delete=models.CASCADE)
