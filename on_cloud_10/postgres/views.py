from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_protect
from .models import Game, Player
import datetime

# Create your views here.

def get_hello_world(request):
    return HttpResponse("Hello World!")

def home_page(request):
    return render(request, "home.html", {})

def new_game(request):
    return render(request, "new_game.html", {})

@csrf_protect
def load_game(request, game_id):
    if request.method == 'POST':

        # print(request.POST)

        # create Game object
        date_time = datetime.datetime.now()
        new_game = Game.objects.create(
            date_of_game=date_time
        )
        new_game.save()

        names = [x for x in request.POST if '_name' in x]
        
        # go through each name input
        # and create Player objects
        for name in names:
            player_name = request.POST.get(name)
            new_player = Player.objects.create(
                name=player_name,
                phase=1,
                score=0,
                game_id=new_game.id
            )
            new_player.save()

        # make a GET request to the same view with the correct game ID
        return redirect("load_game", game_id=new_game.id)
    
    # GET request
    else:
        game = Game.objects.filter(id=game_id)

        # return a 404 screen in the event a user types in a 
        # non-existing game code in the URL
        if not game:
            return render(request, "404_screen.html", {})
        
        players = Player.objects.filter(game_id=game_id).all()
        return render(request, "load_game.html", {"game_id": game_id, "players": players})
    
def validate_game_code(request, game_id):
    if request.method == 'GET':
        game = Game.objects.filter(id=game_id)

        # check if the game exists in Postgres
        if not game:
            return JsonResponse(data={"exists": False})
    
        return JsonResponse(data={"exists": True})
    
@csrf_protect
def save_players_progress(request):
    if request.method == 'POST':
        scores = [x for x in request.POST if '_score' in x]
        phases = [x for x in request.POST if '_phase' in x]
        ids = [x for x in request.POST if '_id' in x]

        for score, phase, id in zip(scores, phases, ids):
            player_score = request.POST.get(score)
            player_phase = request.POST.get(phase)
            player_id = request.POST.get(id)

            Player.objects.filter(id=player_id).update(score=player_score, phase=player_phase)
        
        return JsonResponse({"saved_progress": True})

