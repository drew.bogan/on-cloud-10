// starts at 2 since Phase 10 requires a min. of 2 players
let numbPlayers = 2;

// function for adding players to the 
// new game screen
function addPlayer() {

    // throw an alert message if the user has reached
    // the number of players limit
    if (numbPlayers >= 6) {
        throw alert("Reached maximum player limit");
    }

    // increment the number of players
    numbPlayers++;

    var playerDiv = document.createElement("div");
    playerDiv.className = "col-6";
    playerDiv.id = `player${numbPlayers}`;


    playerDiv.innerHTML = `
    <div style="background-color: rgb(50, 156, 50);">
        <div class="position-relative mb-4 fs-4 text-center">
            Player ${numbPlayers}
            <button type="button" class="btn-close position-absolute top-0 start-0" 
            aria-label="Close" style=".btn-close-color: red;" onclick="deletePlayer()">
            </button>   
        </div>
        <div class="form-floating mb-3" id="p${numbPlayers}_div">
            <input type="text" class="form-control" id="p${numbPlayers}_name" 
            placeholder="" required>
            <label>Enter name</label>
            <div class="invalid-feedback" style="font-weight: 400; 
                color:darkred; font-size: large;" id="p${numbPlayers}_null_value">
                Please enter a name for the player.
            </div>
        </div>
    </div>`;

    var targetRow = document.getElementById("players");

    targetRow.appendChild(playerDiv);
}

function deletePlayer() {

    var targetRow = document.getElementById(`player${numbPlayers}`);
    targetRow.remove();
    numbPlayers--;

}

// function for submitting a form
// through the 'Start game!' button
function submitForm() {

    let currPlayer = 1;

    var form = document.getElementById("player_form");
    var button = document.getElementById("start_game_button");


    // loop through all players
    while (currPlayer <= numbPlayers) {
        var name = `p${currPlayer}_name`;
        var value = document.getElementById(name).value;
        
        // player div tag which holds input
        var div = document.getElementById(`p${currPlayer}_div`);
    
        // check if the name is blank
        if (value == null || value == "") {
            div.classList.add("was-validated");

            // prevent submission
            button.addEventListener("click", function(event) {
                event.preventDefault();
                event.stopPropagation();
            });
        }

        // check if the name is longer than 50 chars
        else if (value.length > 50) {
            if (div.classList.contains("was-validated")) {
                div.classList.remove("was-validated");
            }
            else {
                div.classList.add("was-validated");
            }
            var nullFeedback = document.getElementById(`p${currPlayer}_null_value`);
            div.removeChild(nullFeedback);
            div.innerHTML += (`
            <div class="invalid-feedback" style="font-weight: 400; 
                color:darkred; font-size: large;" id="p${currPlayer}_long_value">
                Name cannot be more than 50 characters long.
            </div>`);
            div.classList.add("was-validated");

            // prevent submission
            button.addEventListener("click", function(event) {
                event.preventDefault();
                event.stopPropagation();
            });
        }

        // add input to form
        else {
            var input = document.createElement("input");
            input.type = "hidden";
            input.id = name + '_id';
            input.name = name;
            input.value = value;
            form.appendChild(input);
        }
    
        currPlayer++;
    }

    // only submit if all fields are valid
    if (form.checkValidity() == true) {
        form.submit();
    }

}