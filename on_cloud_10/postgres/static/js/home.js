// takes in game code input and creates a URL
function constructURL() {
    var gameCode = document.getElementById('game_code').value;

    // AJAX function for validation
    $.ajax({
        url: `validate_game/${gameCode}`,
        method: "GET",
        success: function(data) {

            // submit form if the game code is valid
            if (data.exists) {
                $('#game_code_form').attr('action', `game/${gameCode}/`).trigger('submit');
            }
            else {
                $('#invalid_game_code').text('');
                $('#invalid_game_code').text('The game code you entered does not exist.');
                $('#game_code_validation').addClass("was-validated");
                $('#game_code').val('');
            }
        },
        // handle blank submission
        error: function(data) {
            $('#invalid_game_code').text('');
            $('#invalid_game_code').text('Please enter a game code.');
            $('#game_code_validation').addClass("was-validated");
        }
    });
}