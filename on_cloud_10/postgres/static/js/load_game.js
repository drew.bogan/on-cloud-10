function resetBackgroundColor(button) {

    // reset the state of the button if it's
    // not clicked
    if (!button.classList.contains('highlighted')) {
        button.style.backgroundColor = 'white';
        button.style.color = 'black';
    }
    
    else {
        var player_phases = document.getElementsByName(button.name);
        // console.log(player_phases);

        // reset all of the buttons for a specific player
        // to allow only one button at a time to be highlighted
        for (var i = 0; i < player_phases.length; i++) {
            player_phases[i].classList.remove('highlighted');
            player_phases[i].style.backgroundColor = 'white';
            player_phases[i].style.color = 'black';
        }

        // add highlighted class back to target button
        button.classList.add('highlighted');
        button.style.backgroundColor = 'rgb(233, 81, 81)';
        button.style.color = 'white';
    }

}

// event listener for setting the default phase on load up
window.addEventListener('DOMContentLoaded', function() {
    // go through each player
    for (var x = 1; x <= numbPlayers; x++) {
        var player_phases = this.document.getElementsByName(`p${x}_phase`);

        // go through each phase 
        for (var phase = 0; phase < player_phases.length; phase++) {

            // find the player's current phase and change the styling
            if (player_phases[phase].value == player_phases[phase].id ) {
                player_phases[phase].classList.add('highlighted');
                player_phases[phase].style.backgroundColor = 'rgb(233, 81, 81)';
                player_phases[phase].style.color = 'white';
            }
        }
    }
})

function incrementScore(button) {

    var player = button.id;
    var score = document.getElementById(`p${player}_score`);
    
    var currScore = Number(score.value);
    var newScore = currScore + 5;

    score.value = newScore;
}

function decrementScore(button) {

    var player = button.id;
    var score = document.getElementById(`p${player}_score`);
    
    var currScore = Number(score.value);

    // only decrement when the score is at least 5 to prevent negative scores
    if (currScore >= 5) {
        var newScore = currScore - 5;
        score.value = newScore;
    }
}

// variable for holding the number of valid score inputs
let validPlayerProgressForm;

// function for saving players' phases/scores
function savePlayersProgress() {

    var form = document.getElementById('players_progress');

    validPlayerProgressForm = 0;

    // iterate through each player
    for (var player = 1; player <= numbPlayers; player++) {
        var score = document.getElementById(`p${player}_score`).value;
        var selected_phase = NaN;
        var player_phases = this.document.getElementsByName(`p${player}_phase`);
        for (var i = 0; i < player_phases.length; i++) {

            // find the player's selected phase
            if (player_phases[i].classList.contains('highlighted')) {
                selected_phase = player_phases[i].value;
            }
        }
        var phase_input = document.createElement("input");
        phase_input.type = "hidden";
        phase_input.value = selected_phase;
        phase_input.name = `p${player}_phase`;
        form.appendChild(phase_input);

        var score_input = document.createElement("input");
        score_input.type = "hidden";
        score_input.value = score;
        score_input.name = `p${player}_score`;
        score_input.onsubmit = validatePlayerScore(document.getElementById(`p${player}_score`));
        form.appendChild(score_input);

        var player_id = document.createElement("input");
        player_id.type = "hidden";
        player_id.value = document.getElementById(`p${player}_id`).value;
        player_id.name = `p${player}_id`;
        form.appendChild(player_id);
    }

    // only submit if all fields are valid
    if (validPlayerProgressForm == numbPlayers) {
        $.ajax({
            url: '/save_game/',
            method: 'POST',
            data: $('#players_progress').serialize(),
            success: function() {
                $('#savedNotification').modal('show');
                setTimeout(function() {
                    $('#savedNotification').modal('hide');
                }, 3000);
            }
        });   
    }
}

function validatePlayerScore(button) {
    var value = button.value;

    // check if the score input is invalid
    if (value == "" || value < 0) {
        if (!button.classList.contains('is-invalid')) {
            button.classList.add('is-invalid');
        }
        validPlayerProgressForm--;
    }
    else {
        if (button.classList.contains('is-invalid')) {
            button.classList.remove('is-invalid');
        }
        validPlayerProgressForm++;
    }
}